  <?php
    $jurusan=$_GET['no'];?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Form Nilai</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Form Nilai</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <form action="" method="POST">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Form Nilai</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>Nama Mahasiswa</label>
                  <select class="select2 form-control" name="mhs" style="width: 100%">
                    <?php
                    $cek = mysqli_query($conn,"SELECT * from mahasiswa where jurusan='$jurusan'");?>
                    <?php while ($row2 =mysqli_fetch_array($cek)) { ?>
                      <option value="<?php echo $row2['nim'];?>"><?php echo $row2['nm_mhs'];?></option>
                    <?php }?>
                  </select>
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Nama Dosen</label>
                  <select class="select2 form-control" name="dosen" style="width: 100%;">
                    <?php $nomor=1;
                    $cek = mysqli_query($conn,"SELECT * FROM dosen d inner join matkul m on m.id=d.matkul_id");?>
                    <?php while ($row2 =mysqli_fetch_array($cek)) { ?>
                      <option value="<?php echo $row2['nip'];?>"><?php echo $row2['nm_dosen'];?>-<?php echo $row2['nm_matkul'];?></option>
                    <?php }?>
                  </select>
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">
                  <label>Nilai Mahasiswa</label>
                  <input type="text" name="nilai" id="nilai" class="form-control">
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" name="Save" class="btn btn-block btn-primary">Save</button>
              </div>
    </section>
    </form>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
if(isset($_POST["Save"]))
{
  $mhs=$_POST['mhs'];
  $dosen=$_POST['dosen'];
  $nilai=$_POST['nilai'];
  $hasil = mysqli_query($conn, "SELECT * from dosen where nip='$dosen'");
  $idmatkul=mysqli_fetch_array($hasil);
  $matkul=$idmatkul['matkul_id'];

if(mysqli_query($conn, "INSERT INTO nilai (matkul_id,dosen_id,mhs_id,nilai) values('$dosen','$dosen','$mhs','$nilai')")) {
  echo "<script>location='index.php?ik596=ceknilai';</script>";
  // header('location:index.php?ik596=nilai');
} else {
  echo "<script>location='index.php?ik596=ceknilai';</script>";
}

}
?>