<?php
	if(isset($_GET['ik596'])){
		$page = $_GET['ik596'];

		switch ($page) {
			case 'mhs':
				include "./View/mhs.php";
				break;
			case 'form_mhs':
				include "./View/form_mhs.php";
				break;
			case 'edit_mhs':
				include "./View/edit_mhs.php";
				break;
			case 'hapus_mhs':
				include "./View/hapus_mhs.php";
				break;

			case 'matkul':
				include "./View/matkul.php";
				break;
			case 'form_matkul':
				include "./View/form_matkul.php";
				break;

			case 'dosen':
				include "./View/dosen.php";
				break;
			case 'form_dosen':
				include "./View/form_dosen.php";
				break;

			case 'nilai':
				include "./View/nilai.php";
				break;
			case 'form_nilai':
				include "./View/form_nilai.php";
				break;

			case 'logout':
				include "./View/logout.php";
				break;
			default:
				include "404.php";
				break;
		}
	}else{
		include "content.php";
	}

	?>