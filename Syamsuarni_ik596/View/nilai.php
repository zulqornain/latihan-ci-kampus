<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Nilai</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Nilai</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>NO</th>
                  <th>JURUSAN</th>
                  <th>NAMA JURUSAN</th>
                  <th>ACTION</th>
                </tr>
                </thead>
                <tbody>
                  <?php $nomor=1;
                  $cek = mysqli_query($conn, "SELECT * FROM jurusan group by id_jurusan");?>
                  <?php while ($row =mysqli_fetch_array($cek)) { ?>

                  <tr>
                    <td><?php echo $nomor; ?></td>
                    <td><?php echo $row['id_jurusan']; ?></td>
                    <td><?php echo $row['jurusan']; ?></td>
                    <td>
                      <a href="?ik596=form_nilai&no=<?php echo $row['id_jurusan'];?>"><button class="btn-warning btn">Pilih</button></a>
                    </td>
                  </tr>
    <?php $nomor++; ?>
  <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->