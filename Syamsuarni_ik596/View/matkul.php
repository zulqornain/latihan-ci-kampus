<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Mata Kuliah</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Mata Kuliah</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title"><a href="?ik596=form_matkul"><button type="button" class="btn btn-block btn-primary btn-lg">Add Mata Kuliah</button></a></h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>NO</th>
                  <th>Nama Mata Kuliah</th>
                  <th>ACTION</th>
                </tr>
                </thead>
                <tbody>
                  <?php $nomor=1;
                  $cek = mysqli_query($conn, "SELECT * FROM matkul");?>
                  <?php while ($row =mysqli_fetch_array($cek)) { ?>

                  <tr>
                    <td><?php echo $nomor; ?></td>
                    <td><?php echo $row['nm_matkul']; ?></td>
                    <td>
                      <a href="?ik596=hapus_matkul&no=<?php echo $row['id'];?>"><button class="btn-danger btn">Hapus</button></a>

                      <a href="?ik596=edit_matkul&no=<?php echo $row['id'];?>"><button class="btn-warning btn">Ubah</button></a>
                    </td>
                  </tr>
    <?php $nomor++; ?>
  <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->