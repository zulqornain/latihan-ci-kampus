<?php
    $id=$_GET['no'];
    $hasil = mysqli_query($conn, "SELECT m.id,m.nim,m.nm_mhs,m.kelas,j.jurusan
      from mahasiswa m
      inner join jurusan j on j.id_jurusan=m.jurusan where m.id='$id'");
    $row=mysqli_fetch_array($hasil);
  ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Form Mahasiswa</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Form Mahasiswa</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <form action="" method="POST">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Form Mahasiswa</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-remove"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>NIM</label>
                  <input type="text" value="<?php echo $row['nim'];?>" name="nim" id="nim" class="form-control">
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                  <label>Nama Mahasiswa</label>
                  <input type="text" value="<?php echo $row['nm_mhs'];?>" name="mhs" id="mhs" class="form-control">
                </div>
                <!-- /.form-group -->
              </div>
              <!-- /.col -->
              <div class="col-md-6">
                <div class="form-group">
                  <label>Jurusan</label>
                  <select class="select2 form-control" name="jrs" data-placeholder="Select a State" style="width: 100%;"> 
                    <option value="<?php echo $row['id_jurusan'];?>">
                      <?php echo $row['jurusan'];?></option>
                    <option value="IK">Informatika Komputer</option>
                    <option value="KA">Komputerisasi Akuntansi</option>
                    <option value="AP">Administrasi Perkantoran</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Kelas</label>
                  <input type="text" value="<?php echo $row['kelas'];?>" name="kls" id="kls" class="form-control">
                </div>
              </div>
              <div class="card-footer">
                <button type="submit" name="Save" class="btn btn-block btn-primary">Save</button>
              </div>
    </section>
    </form>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
if(isset($_POST["Save"]))
{
  $nim=$_POST['nim'];
  $mhs=$_POST['mhs'];
  $jrs=$_POST['jrs'];
  $kls=$_POST['kls'];
if(mysqli_query($conn, "UPDATE mahasiswa SET nim='$nim',nm_mhs='$mhs',jurusan='$jrs',kelas='$kls' WHERE id=$id")) {
  echo "<script>location='index.php?ik596=mhs';</script>";
  // header('location:index.php?ik596=mhs');
} else {
  echo "<script>location='index.php?ik596=mhs';</script>";
}

}
?>