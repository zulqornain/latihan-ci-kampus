    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Data Mahasiswa</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">DataTables</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title"> 
                  <button  onclick="adddata()"   type="button" class="btn btn-block btn-primary">Add Mahasiswa</button> 
                </h3>
              </div>
              <!-- /.card-heik596=form-mhsader -->
              <div class="card-body">
                <table id="table" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>NIM</th>
                    <th>Nama Mahasiswa</th>
                    <th>Jurusan</th>
                    <th>Kelas</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    
                  </tbody> 
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
   
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
<script src="<?php echo $base; ?>Asset/plugins/jquery/jquery.min.js"></script>
 <script>
   var Datamahasiswa = {
        table: undefined
    };
    $(document).ready(function () {
        /* start datatable */
        var handleDataTableDefault = function () {
            if ($("#table").length!==0) {
               Datamahasiswa.table = $("#table").DataTable({
                    "fnRowCallback" : function(nRow, aData, iDisplayIndex) {
                        var oSettings = this.fnSettings();
                        var iTotalRecords = oSettings.fnRecordsDisplay();
                        $("#total").val(iTotalRecords);
                        $("td:first", nRow).html(iDisplayIndex +1);
                       return nRow;
                    },
                    "ajax": {
                        "url": '<?php echo $url_grid; ?>',
                        "type": 'POST',
                    },
                    "columns": [
                        {"data": "id"},
                        {"data": "nim"},
                        {"data": "nm_mhs"}, 
                        {"data": "jurusan"}, 
                        {"data": "kelas"}, 
                        {
                            "data": "id", "width": "100px", "sClass": "left",
                            "bSortable": false,
                            "mRender": function (data, type, row) {
                                var btn = "";
                               
                                btn = btn + " <button onClick='ParamFunc.editdata(" + row.id + ")' class='btn btn-xs btn-icon btn-circle btn-warning' data-toggle='tooltip' data-placement='bottom' title='Edit Data' type='button'><i class='fa fa-edit'></i> </button>"; 

                                btn = btn + " <button onClick='ParamFunc.deletedata(" + row.id + ")' class=' btn btn-xs btn-icon btn-circle btn-danger' data-toggle='tooltip' data-placement='bottom' title='Delete Data' type='button'>X </button>";
                                
                                btn = btn + "</div>";
                                return btn;
                            }
                        }
                    ]
                });
            }
        };
        TableManageDefault = function () {
            "use strict";
            return {
                init: function () {
                    handleDataTableDefault();
                }
            };
        }();
        TableManageDefault.init();
    });
    /* end datatable */



    /*start edit, delete  function */
    var ParamFunc = {
        editdata: function (id) {
            window.top.location.href = 'Datamahasiswa/edit/'+id; 
        }, 
        deletedata: function (id) {
              var jawab = confirm("Apakah anda yakin untuk menghapus !");
                if (jawab === true) {
        //            kita set hapus false untuk mencegah duplicate request
                    var hapus = false;
                    if (!hapus) {
                        hapus = true;
                        $.post('<?php echo $url_delete ?>', {id: id},
                        function (data) {
                           Datamahasiswa.table.ajax.reload();
                        });
                        hapus = false;
                    }
                } else {
                    return false;
                }
        }
    } 
    /*start add function */
    function adddata() {
        window.top.location.href = '<?php echo $url_addmhs ?>'; 
    }
    /*end add function */

   

</script>
 
    