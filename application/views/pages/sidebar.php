  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
  

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo $base; ?>Asset/dist/img/Koala.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Alif Zulqornain</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Master Data
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo $base; ?>index.php/Datamahasiswa" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Mahasiswa</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="?ik596=matkul" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Mata Kuliah</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="?ik596=dosen" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Dosen</p>
                </a>
              </li>
              <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Transaksi Nilai
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
              </li>
              <li class="nav-item">
                <a href="?ik596=nilai" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Form Nilai</p>
                </a>
              </li>
            <li class="nav-item">
                <a href="?ik596=nilai" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Cek Nilai Mahasiswa</p>
                </a>
              </li>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>