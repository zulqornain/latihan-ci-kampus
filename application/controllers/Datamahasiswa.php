<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Datamahasiswa extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Mahasiswa','mhs');
	}
	public function index(){
		$data = array(
			"base" => base_url(),
			"page" => "mahasiswa",
			"url_grid" => "Datamahasiswa/grid",
			"url_addmhs" => "Datamahasiswa/addmahasiswa",
			"url_delete" => "Datamahasiswa/remove"
		);

		$this->load->view('template/index',$data);
	}
	public function grid() {
		echo json_encode(array(
			"data" => $this->mhs->getGridData()->result()
		));

	}
}
