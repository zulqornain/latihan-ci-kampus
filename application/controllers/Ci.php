<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ci extends CI_Controller {
	function __construct() {
		parent::__construct();
	}
	public function index(){
		$data = array(
			"base" => base_url(),
			"urlbase" => "index.php",
			"page" => "pages/content"
		);

		$this->load->view('template/index',$data);
	}
}
